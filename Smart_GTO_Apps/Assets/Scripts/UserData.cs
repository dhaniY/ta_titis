﻿using System;
using System.Collections.Generic;


//data struct
//data ini nanti nya untuk menampung data json yang di dapatkan dari database,
//data yang di simpan adalah data transaksi, balance data.
namespace SmartGTO
{
    [Serializable]
    public struct Data
    {
        public string tolIn;
        public string tolOut;
        public string price;
        public string created_at;
        public string beacon;
    }
    [Serializable]
    public struct DataUser
    {
        public int balance;
        public List<Data> data;
    }
}
