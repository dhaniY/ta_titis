﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

// apps data handler class 
// berfungsi sebagai untuk meng handle atau menangani data cache user
public class AppsDataHandler : MonoBehaviour
{
    // Deklarasi singleton class, agar apps data handler dapat dipakai disemua class, dan reuseable.
    private static AppsDataHandler _instance;
    public static AppsDataHandler Instance
    {
        get
        {
            return _instance;
        }
    }

    public GameObject loginOBJ;
    public GameObject MainMenuOBJ;
    public TextMeshProUGUI UserName;
    public Manager manager;
    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        //Pengecekkan status login
        //diambil data chache dengan memanggil playerpref.getint dengan nilai key "StatusLogin" nilai default 0.
        //nilai default befungsi jika "StatusLogin" tidak memiliki nilai.
        //jika nilai "StatusLogin" Berniliai 1 maka user sudah melakukan login
        if (PlayerPrefs.GetInt("StatusLogin",0) == 1)
        {
            loginOBJ.SetActive(false);
            MainMenuOBJ.SetActive(true);
            //mengisi nama user dari data chache.
            UserName.text = PlayerPrefs.GetString("UserName");
            StartCoroutine(manager.GetData());
        }
        else
        {
            loginOBJ.SetActive(true);
            MainMenuOBJ.SetActive(false);
        }
    }
}
