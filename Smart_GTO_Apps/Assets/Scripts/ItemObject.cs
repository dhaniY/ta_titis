﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

//Item Object, Digunakan untuk menampilkan transaksi.
public class ItemObject : MonoBehaviour
{
    public string tolIn_Item;
    public string tolOut_Item;
    public string price_Item;
    public string created_at_Item;

    public GameObject tolIn_Object;
    public GameObject tolOut_Object;
    public GameObject price_Object;
    public GameObject created_at_Object;
    // Start is called before the first frame update
    void Start()
    {
        tolIn_Object.GetComponent<TextMeshProUGUI>().text = tolIn_Item;
        tolOut_Object.GetComponent<TextMeshProUGUI>().text = tolOut_Item;
        price_Object.GetComponent<TextMeshProUGUI>().text = price_Item;
        created_at_Object.GetComponent<TextMeshProUGUI>().text = created_at_Item;
    }
}
