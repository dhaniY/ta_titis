﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

// class Login Handler, semua proses login dan user logout di lakukan di class ini
public class LoginHandler : MonoBehaviour
{
    private string[] username = { "titis", "reyhan", "dhani"};
    private string password = "1234";
    public GameObject loadingObj;
    public GameObject loginOBJ;
    public GameObject mainMenuOBJ;
    //Object Input Field
    public TMP_InputField usernameInput;
    public TMP_InputField passwordInput;
    public TextMeshProUGUI errorText;
    public Manager manager;
    string tempUserName;

    private void Start()
    {
        loadingObj.SetActive(false);
        errorText.gameObject.SetActive(false);
    }
    //Fungsi Listener button saat click login
    public void OnClickLogin()
    {
        if (usernameInput.text == "" || passwordInput.text == "")
        {
            errorText.gameObject.SetActive(true);
            errorText.text = "*There is a Blank field";
            return;
        }
        StartCoroutine(login());
    }

    //fungsi listner button saat click logout
    public void OnClickLogOut()
    {
        //data chache "StatusLogin" di isi dengan 0, artinya belum login
        PlayerPrefs.SetInt("StatusLogin", 0);
        //data Chace "UserName" dikosongi.
        PlayerPrefs.SetString("UserName", "");
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    //melakukan proses login, dan pengecekan user.
    IEnumerator login()
    {
        loadingObj.SetActive(true);
        bool success = CheckUserName();
        bool succesPassword = CheckPassword();
        yield return new WaitForSeconds(3f);
        if (success && succesPassword)
        {
            loadingObj.SetActive(false);
            loginOBJ.SetActive(false);
            mainMenuOBJ.SetActive(true);
            PlayerPrefs.SetInt("StatusLogin",1);
            PlayerPrefs.SetString("UserName", tempUserName);
            AppsDataHandler.Instance.UserName.text = tempUserName;
            StartCoroutine(manager.GetData());
        }
        else
        {
            loadingObj.SetActive(false);
            errorText.gameObject.SetActive(true);
            errorText.text = "*wrong ussername or password";
        }
    }
    //pengecekkan apakah password sesuai
    private bool CheckPassword()
    {
        if (passwordInput.text == password)
        {
            return true;
        }
        return false;
    }
    //pengecekkan username sesuai yang sudah terdaftar
    bool CheckUserName()
    {
        for (int i = 0; i<username.Length; i++)
        {
            if (usernameInput.text == username[i])
            {
                tempUserName = usernameInput.text;
                return true;
            }
        }
        return false;
    }
}
