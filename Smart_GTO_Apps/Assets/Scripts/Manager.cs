﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SmartGTO;
using UnityEngine.Networking;
using TMPro;


//Manager disini berperan penting untuk melakukan post get dengan database.
public class Manager : MonoBehaviour
{
    private string _BeaconTitis = "ac:23:3f:23:5a:e0";
    private string _BeaconReyhan = "ac:23:3f:24:9f:b5";
    private string _BeaconDhani = "ac:23:3f:23:5b:c2";

    [SerializeField]
    DataUser UserData;
    string url = "http://deploy-testing.sepaystudio.com/transaction-data.php" ;
    public GameObject balance_Object;

    public List<GameObject> listTransaction;
    public GameObject prefabItem;
    public Transform itemTransform;

    public TopUpHandler topUpHandler;

    private int tempSUM;
    
    //fungsi get datauser
    public IEnumerator GetData()
    {
        UnityWebRequest www = UnityWebRequest.Get(url);
        var get = www.SendWebRequest();
        Debug.Log("Loading...");
        while (!get.isDone) yield return new WaitForEndOfFrame();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            // Show results as text
            Debug.Log(www.downloadHandler.text);
            UserData = JsonUtility.FromJson<DataUser>(www.downloadHandler.text);
            topUpHandler.GetTopUp();
            UpdateData();
        }
    }

    //fungsi untuk update balance 
    public void UpdateBalance()
    {
        int balance = (UserData.balance + topUpHandler.tempTopUp) - tempSUM;
        balance_Object.GetComponent<TextMeshProUGUI>().text = "Rp. " + balance.ToString();
    }


    //fungsi untuk update data transaksi berdasarkan user
    private void UpdateData()
    {   
        if (listTransaction.Count != UserData.data.Count)
        {
            int listCount = listTransaction.Count;
            for (int i = listCount; i < UserData.data.Count; i++)
            {
                if (PlayerPrefs.GetString("UserName") == "titis" && UserData.data[i].beacon == _BeaconTitis)
                {
                    GameObject instant = Instantiate(prefabItem, itemTransform.transform.localPosition, Quaternion.identity);
                    instant.transform.SetParent(itemTransform);
                    instant.transform.localScale = new Vector3(1, 1, 1);
                    instant.GetComponent<ItemObject>().tolIn_Item = UserData.data[i].tolIn;
                    instant.GetComponent<ItemObject>().tolOut_Item = UserData.data[i].tolOut;
                    instant.GetComponent<ItemObject>().price_Item = "Rp. " + UserData.data[i].price;
                    tempSUM += int.Parse(UserData.data[i].price);
                    instant.GetComponent<ItemObject>().created_at_Item = UserData.data[i].created_at;
                    listTransaction.Add(instant);
                    Debug.Log("titis");
                }
                else if (PlayerPrefs.GetString("UserName") == "reyhan" && UserData.data[i].beacon == _BeaconReyhan)
                {
                    GameObject instant = Instantiate(prefabItem, itemTransform.transform.localPosition, Quaternion.identity);
                    instant.transform.SetParent(itemTransform);
                    instant.transform.localScale = new Vector3(1, 1, 1);
                    instant.GetComponent<ItemObject>().tolIn_Item = UserData.data[i].tolIn;
                    instant.GetComponent<ItemObject>().tolOut_Item = UserData.data[i].tolOut;
                    instant.GetComponent<ItemObject>().price_Item = "Rp. " + UserData.data[i].price;
                    tempSUM += int.Parse(UserData.data[i].price);
                    instant.GetComponent<ItemObject>().created_at_Item = UserData.data[i].created_at;
                    listTransaction.Add(instant);
                    Debug.Log("Reyhan");
                }
                else if(PlayerPrefs.GetString("UserName") == "dhani" && UserData.data[i].beacon == _BeaconDhani)
                {
                    GameObject instant = Instantiate(prefabItem, itemTransform.transform.localPosition, Quaternion.identity);
                    instant.transform.SetParent(itemTransform);
                    instant.transform.localScale = new Vector3(1, 1, 1);
                    instant.GetComponent<ItemObject>().tolIn_Item = UserData.data[i].tolIn;
                    instant.GetComponent<ItemObject>().tolOut_Item = UserData.data[i].tolOut;
                    instant.GetComponent<ItemObject>().price_Item = "Rp. " + UserData.data[i].price;
                    tempSUM += int.Parse(UserData.data[i].price);
                    instant.GetComponent<ItemObject>().created_at_Item = UserData.data[i].created_at;
                    listTransaction.Add(instant);
                    Debug.Log("else");
                }
            }
            UpdateBalance();
        }
    }
}
