﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

// Class untuk melakukan top up
public class TopUpHandler : MonoBehaviour
{
    // object input field
    public TMP_InputField nominalInput;
    // object jika ada pesan error
    public GameObject errorOBJ;
    // object loading
    public GameObject loadingOBJ;
    public Manager manager;
    public GameObject popUpOBJ;
    //variabel untuk menyimpan top up sementara
    public int tempTopUp;
    //fungsi awake dijalankan saat pertama kali aplikasi dijalankan
    private void Awake()
    {
        errorOBJ.SetActive(false);
        popUpOBJ.SetActive(false);
        GetTopUp();
    }
    //mengambil jumlah top up sesuai user
    public void GetTopUp()
    {
        if (PlayerPrefs.GetString("UserName").ToLower() == "titis")
        {
            tempTopUp = PlayerPrefs.GetInt("titistopup",0);
            Debug.Log(tempTopUp);
        }
        else if (PlayerPrefs.GetString("UserName").ToLower() == "reyhan")
        {
            tempTopUp = PlayerPrefs.GetInt("reyhantopup",0);
            Debug.Log(tempTopUp);
        }
        else if (PlayerPrefs.GetString("UserName").ToLower() == "dhani")
        {
            tempTopUp = PlayerPrefs.GetInt("dhanitopup",0);
            Debug.Log(tempTopUp);
        }
    }

    //menyimpan jumlah top up yang dimasukkan kedalam memory
    void SetTopUp()
    {
        if (PlayerPrefs.GetString("UserName").ToLower() == "titis")
        {
            PlayerPrefs.SetInt("titistopup", tempTopUp);
            Debug.Log(tempTopUp);
        }
        else if (PlayerPrefs.GetString("UserName").ToLower() == "reyhan")
        {
            PlayerPrefs.SetInt("reyhantopup",tempTopUp);
            Debug.Log(tempTopUp);
        }
        else if (PlayerPrefs.GetString("UserName").ToLower() == "dhani")
        {
            PlayerPrefs.SetInt("dhanitopup",tempTopUp);
            Debug.Log(tempTopUp);
        }
        manager.UpdateBalance();
    }

    //fungsi button top up
    public void OnTopUpClick()
    {
        if (nominalInput.text == "")
        {
            errorOBJ.SetActive(true);
            return;
        }
        StartCoroutine(TopUP());
    }

    // menjalankan proses top up
    IEnumerator TopUP()
    {
        loadingOBJ.SetActive(true);
        yield return new WaitForSeconds(3);
        loadingOBJ.SetActive(false);
        tempTopUp += int.Parse(nominalInput.text);
        SetTopUp();
        popUpOBJ.SetActive(true);
    }
}
